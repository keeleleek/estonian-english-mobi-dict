# Estonian-English-mobi-dict

Project for creating a usable Estonian to English dictionary for the kindle and other e-readers that support the .mobi format. 

To generate from scratch have your tab delinated data in ET-EN.txt

```
sh tab2obf.sh
apply ET-EN.obf ET-EN.obf.patch # Edits the title  adds cover + in-out lang metadata
sh mobigen.sh
```

`snippit.html` contains a sample of a working entery with various word-forms

`synthesize.py` contains a simple script inorder to use estnltk to generate the word-forms. 

Ideally we should use bs4 or some other xml parser to parse the generated hmtl files and add the `<iform>` tags.
